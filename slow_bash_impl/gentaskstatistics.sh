#!/bin/bash

IFS='
'

tasksdir="$1"
if test -z "$tasksdir"
then
  tasksdir="db_tasks"
fi

function fail() {
  >&2 echo "$1"
  exit 2
}

mkdir -p "db_info"

function countdownloaded() {
  local url
  local c=0
  while read url
  do
    if  test -f "db/$url/index.html" && \
        test -f "db/$url/index_hdr" && \
      ! test -f "db/$url/err"
    then
      c=$(( $c + 1 ))
    fi
  done
  echo $c
}


if test -d "$tasksdir"
then
  cat /dev/null >db_info/tasks_progress
  for t in `ls "$tasksdir"`
  do
    echo "generating statistics for $t"
    urlcount="`cat $tasksdir/$t | wc -l`"
    dwnldcount="`cat $tasksdir/$t | countdownloaded`"
    prcnt=$(( $dwnldcount * 100 / $urlcount ))
    echo "$t $dwnldcount/$urlcount ($prcnt%)" >>db_info/tasks_progress
  done
  echo "results were saved to \"db_info/tasks_progress\""

  # report to stdin
  echo ""
  cat db_info/tasks_progress
else
  fail "no \"$tasksdir/\" directory was found"
fi

