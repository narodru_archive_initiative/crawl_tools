local file = {
  read = function (fname)
    local fd = io.open(fname, "rb")
    if not fd then
      return nil
    end
    local data = fd:read("*a")
    fd:close()
    return data
  end
}

for fname in io.lines() do
  if fname ~= "" then
    local data = file.read(fname)
    if not data then
      error("no such file")
    end
    for ip in string.gmatch(data, "%d+%.%d+%.%d+%.%d+") do
      print(fname, ip)
    end
  end
end

