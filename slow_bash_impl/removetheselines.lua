local removefrom = assert(arg[1])
local theselines = assert(arg[2])

local flines = {}
for line in io.lines(theselines) do
  flines[line] = true
end

for line in io.lines(removefrom) do
  if not flines[line] then
    print(line)
  end
end

