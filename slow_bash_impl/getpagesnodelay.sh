#!/bin/bash

uagent="Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0"

function fail() {
  >&2 echo "$1"
  exit 2
}

function err() {
  >&2 echo "$1"
}

function getpage() {

  if curl -A "$uagent" -D index_hdr -o index.html -- "http://$url/" >out 2>err
  then
    rm out err
  else
    err "curl error ($?) `cat err`"
  fi
}



while read url
do

  # skip empty lines
  if test -z "$url"
  then
    continue
  fi

  mkdir -p "db/$url"
  pushd "db/$url" >/dev/null || fail "pushd error"
  if test -f err
  then
    echo "re-downloading $url"
    getpage "$url"
  else
    if test -f index_hdr && test -f index.html
    then
      echo "skipping $url"
    else
      echo "getting $url"
      getpage "$url"
    fi
  fi
  popd >/dev/null || fail "popd error"
done

