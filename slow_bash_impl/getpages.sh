#!/bin/bash

# download URLs and save index.html and response HTTP header
# to db/ directory in format:
#   db/_url_/index.html
#   db/_url_/index_hdr
#
# usage:
#   cat urls_one_per_line.txt | bash getpages.sh

uagent="Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0"
delay=30s

function fail() {
  >&2 echo "$1"
  exit 2
}

function err() {
  >&2 echo "$1"
}

downloaded=0

function getpage() {

  # wait between requests
  if test $downloaded = 0
  then
    downloaded=1
  else
    echo "waiting for $delay before downloading..."
    sleep $delay || exit 1
  fi

  if curl -A "$uagent" -D index_hdr -o index.html -- "http://$url/" >out 2>err
  then
    rm out err
  else
    err "curl error ($?) `cat err`"
  fi
}



while read url
do

  # skip empty lines
  if test -z "$url"
  then
    continue
  fi

  mkdir -p "db/$url"
  pushd "db/$url" >/dev/null || fail "pushd error"
  if test -f err
  then
    echo "re-downloading $url"
    getpage "$url"
  else
    if test -f index_hdr && test -f index.html
    then
      echo "skipping $url"
    else
      echo "getting $url"
      getpage "$url"
    fi
  fi
  popd >/dev/null || fail "popd error"
done

