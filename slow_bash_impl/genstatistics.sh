#!/bin/bash

IFS='
'

function fail() {
  >&2 echo "$1"
  exit 2
}

count200=0
count404=0
count410=0
count503=0
count301=0
countunknwn=0
counterr=0
countall=0

mkdir -p "db_info"
cat /dev/null >db_info/summary
cat /dev/null >db_info/pages_failed
cat /dev/null >db_info/pages_status_404
cat /dev/null >db_info/pages_status_410
cat /dev/null >db_info/pages_status_503
cat /dev/null >db_info/pages_status_200
cat /dev/null >db_info/pages_status_301
cat /dev/null >db_info/pages_status_unknwn
cat /dev/null >db_info/pages_status_unknwn_human

while read dir
do
  # skip empty lines
  if test -z "$dir"
  then
    continue
  fi

  pcountall=$(( $countall + 1 ))
  echo "processing ${pcountall}. $dir"

  pushd "db/$dir" >/dev/null || fail "pushd error"
  if test -f err
  then
    counterr=$(( $counterr + 1 ))
    echo "$dir" >>../../db_info/pages_failed
    countall=$(( $countall + 1 ))
    popd >/dev/null || fail "popd error"
    continue
  fi

  headerline1="`head -n1 index_hdr`"

  if echo "$headerline1" | grep -ic 'HTTP/1.1 404 Not Found' >/dev/null
  then
    count404=$(( $count404 + 1 ))
    echo "$dir" >>../../db_info/pages_status_404
    countall=$(( $countall + 1 ))
    popd >/dev/null || fail "popd error"
    continue

  elif echo "$headerline1" | grep -ic 'HTTP/1.1 301 Moved Permanently' >/dev/null
  then
    count301=$(( $count301 + 1 ))
    echo "$dir" >>../../db_info/pages_status_301
    countall=$(( $countall + 1 ))
    popd >/dev/null || fail "popd error"
    continue

  elif echo "$headerline1" | grep -ic 'HTTP/1.1 410 Not found' >/dev/null
  then
    count410=$(( $count410 + 1 ))
    echo "$dir" >>../../db_info/pages_status_410
    countall=$(( $countall + 1 ))
    popd >/dev/null || fail "popd error"
    continue

  elif echo "$headerline1" | grep -ic 'HTTP/1.1 503 ' >/dev/null
  then
    count503=$(( $count503 + 1 ))
    echo "$dir" >>../../db_info/pages_status_503
    countall=$(( $countall + 1 ))
    popd >/dev/null || fail "popd error"
    continue

  elif echo "$headerline1" | grep -ic 'HTTP/1.1 200 OK' >/dev/null
  then
    count200=$(( $count200 + 1 ))
    echo "$dir" >>../../db_info/pages_status_200
    countall=$(( $countall + 1 ))
    popd >/dev/null || fail "popd error"
    continue

  else
    countunknwn=$(( $countunknwn + 1 ))
    echo "$dir"              >>../../db_info/pages_status_unknwn
    printf "%-35s %s\n" "$dir" "$headerline1" >>../../db_info/pages_status_unknwn_human
    countall=$(( $countall + 1 ))
    popd >/dev/null || fail "popd error"
    continue
  fi
done

echo "total records: $countall" >>db_info/summary
echo "status 200 OK                 count: $count200" >>db_info/summary
echo "status 404 Not Found          count: $count404" >>db_info/summary
echo "status 410 Gone               count: $count410" >>db_info/summary
echo "status 301 Moved Permanently  count: $count301" >>db_info/summary
echo "status 503 Tmprrly Unavilable count: $count503" >>db_info/summary
echo "status unknown count: $countunknwn" >>db_info/summary
echo "download failures: $counterr" >>db_info/summary

echo ""
cat db_info/summary
echo ""
echo "results were saved to db_info/* files"

