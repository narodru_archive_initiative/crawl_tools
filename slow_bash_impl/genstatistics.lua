#!/usr/bin/env lua

local file = require "pl.file"
local dir = require "pl.dir"
local pretty = require "pl.pretty"
local path = require "pl.path"
local stringx = require "pl.stringx"
require "lfs"

local function printf (...)
  print(string.format(...))
end

local function fappend (fname, str)
  local fd = io.open(fname, "ab")
  if not fd then return nil end
  if not fd:write(str, "\n") then
    fd:close()
    return nil
  end
  if not fd:close() then
    return nil
  end
  return true
end

local function fappendf (fname, ...)
  return fappend(fname, string.format(...))
end

local function fline1 (fname)
  local fd = io.open(fname, "rb")
  if not fd then return nil, "cannot open" end
  local line = fd:read()
  if not line then
    fd:close()
    return nil, "no line"
  end
  if not fd:close() then
    return nil, "cannot close"
  end
  return line
end


local count200 = 0
local count404 = 0
local count410 = 0
local count503 = 0
local count301 = 0
local countunknwn = 0
local counterr = 0
local countall = 0

for i, l in ipairs {
  "mkdir -p 'db_info'",
  "cat /dev/null >db_info/summary",
  "cat /dev/null >db_info/pages_failed",
  "cat /dev/null >db_info/pages_status_404",
  "cat /dev/null >db_info/pages_status_410",
  "cat /dev/null >db_info/pages_status_503",
  "cat /dev/null >db_info/pages_status_200",
  "cat /dev/null >db_info/pages_status_301",
  "cat /dev/null >db_info/pages_status_unknwn",
  "cat /dev/null >db_info/pages_status_unknwn_human",
} do print("executing", l) os.execute(l) end

local dir = io.read()
while dir do
  if dir and #dir > 0 then

    pcountall = countall + 1
    printf("processing %d. %s", pcountall, dir)

    local dirdir = "db/"..dir

    if path.isfile(dirdir.."/err")
    or not path.isfile(dirdir.."/index.html")
    or not path.isfile(dirdir.."/index_hdr")
    then
      counterr = counterr + 1
      assert(fappend("db_info/pages_failed", dir))
      countall = countall + 1
    else

      local headerline1 = assert(fline1(dirdir.."/index_hdr"))

      if string.match(string.lower(headerline1), "http/1%.%d%s+404%s+not%s+found") then
        count404 = count404 + 1
        assert(fappend("db_info/pages_status_404", dir))
        countall = countall + 1

      elseif string.match(string.lower(headerline1),
          "http/1%.%d%s+301%s+moved%s+permanently") then
        count301 = count301 + 1
        assert(fappend("db_info/pages_status_301", dir))
        countall = countall + 1

      elseif string.match(string.lower(headerline1),
          "http/1%.%d%s+410%s+not%s+found") then
        count410 = count410 + 1
        assert(fappend("db_info/pages_status_410", dir))
        countall = countall + 1

      elseif string.match(string.lower(headerline1), "http/1%.%d%s+503%s+") then
        count503 = count503 + 1
        assert(fappend("db_info/pages_status_503", dir))
        countall = countall + 1

      elseif string.match(string.lower(headerline1), "http/1%.%d%s+200%s+ok") then
        count200 = count200 + 1
        assert(fappend("db_info/pages_status_200", dir))
        countall = countall + 1

      else
        countunknwn = countunknwn + 1
        assert(fappend("db_info/pages_status_unknwn", dir))
        assert(fappendf("db_info/pages_status_unknwn_human", "%-35s %s", dir, headerline1))
        countall = countall + 1

      end
    end
  end
  dir = io.read()
end

local sumf = "db_info/summary"
fappendf(sumf, "total records: %d", countall)
fappendf(sumf, "status 200 OK                 count: %d", count200)
fappendf(sumf, "status 404 Not Found          count: %d", count404)
fappendf(sumf, "status 410 Gone               count: %d", count410)
fappendf(sumf, "status 301 Moved Permanently  count: %d", count301)
fappendf(sumf, "status 503 Tmprrly Unavilable count: %d", count503)
fappendf(sumf, "status unknown count: %d", countunknwn)
fappendf(sumf, "download failures: %d", counterr)

print()
print(file.read("db_info/summary"))
print("results were saved to db_info/* files")

