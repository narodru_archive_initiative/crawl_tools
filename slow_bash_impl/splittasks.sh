#!/bin/bash

IFS='
'

tasksize="$1"
if -z "$tasksize"
then
  tasksize=5000
fi

function fail() {
  >&2 echo "$1"
  exit 2
}

count=0
countall=0
curtaskno=1

mkdir -p "db_tasks"
cat /dev/null >db_tasks/task${curtaskno}

while read url
do
  # skip empty lines
  if test -z "$url"
  then
    continue
  fi

  echo "processing ${countall}. $url"

  count=$(( $count + 1 ))
  countall=$(( $countall + 1 ))
  echo "$url" >>db_tasks/task${curtaskno}
  if test $count -ge $tasksize
  then
    count=0
    curtaskno=$(( $curtaskno + 1 ))
    cat /dev/null >db_tasks/task${curtaskno}
  fi
done

